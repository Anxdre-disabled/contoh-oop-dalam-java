package com.company;

import java.util.Scanner;

public class Main {
    private static Drone mDrone = new Drone();

    public static void main(String[] args) {
        showMenu();
    }

    private static void showMenu() {
        Scanner mScanner = new Scanner(System.in);
        System.out.println("\n1.hidupkan mesin" +
                "\n2.matikan mesin" +
                "\n3.isi daya" +
                "\n4.naik" +
                "\n5.turun" +
                "\n6.maju" +
                "\n7.mundur" +
                "\n8.belok" +
                "\n\n Energi drone saat ini : " + mDrone.getEnergi() +
                "\n Kecepatan drone saat ini : " + mDrone.getKecepatan() +
                "\n Ketinggian drone saat ini : " + mDrone.getKetinggian() +
                "\n Aktivitas drone saat ini : " + mDrone.getAktifitas());
        String menu = mScanner.nextLine();

        switch (menu) {
            case "1":
                mDrone.hidupkanMesin();
                break;
            case "2":
                mDrone.matikanMesin();
                break;
            case "3":
                System.out.print("Masukkan jumlah energi yang akan diisi : ");
                mDrone.isiEnergiDrone(mScanner.nextInt());
                break;
            case "4":
                mDrone.naik();
                break;
            case "5":
                mDrone.turun();
                break;
            case "6":
                mDrone.maju();
                break;
            case "7":
                mDrone.mundur();
            case "8":
                mDrone.belok();
                break;
        }
        showMenu();
    }
}
