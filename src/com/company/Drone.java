package com.company;

@SuppressWarnings("WeakerAccess")
class Drone {
    private int kecepatan, ketinggian, energi = 0;
    private String aktifitas = "mati";

    public int getEnergi() {
        return energi;
    }

    public void setEnergi(int energi) {
        this.energi = energi;
    }

    public int getKecepatan() {
        return kecepatan;
    }

    public int getKetinggian() {
        return ketinggian;
    }

    String getAktifitas() {
        return aktifitas;
    }

    void turun() {
        if (aktifitas.equals("mati")) {
            System.out.println("Drone saat ini dalam kondisi mati");
        } else {
            energi = energi - 5;
            kecepatan = kecepatan - 10;
            ketinggian = ketinggian - 20;
            if (ketinggian <= 0) {
                kecepatan = 3;
                ketinggian = 0;
            }
            System.out.println("Drone turun");
        }
        cekAktifitas();
    }

    void matikanMesin() {
        if (aktifitas.equals("mati")) {
            System.out.println("Mesin telah mati");
        } else {
            if (ketinggian <= 0 && kecepatan <= 0) {
                aktifitas = "mati";
                System.out.println("Mesin Dimatikan");
            } else System.out.println("Drone tidak bisa dimatikan");
        }
    }

    void hidupkanMesin() {
        if (aktifitas.equals("mati")) {
            kecepatan = 3;
        } else {
            System.out.println("Mesin telah menyala");
        }
        cekAktifitas();
    }

    void isiEnergiDrone(int daya) {
        if (aktifitas.equals("mati")) {
            energi = energi + daya;
            System.out.println("Energi berhasil di isi");
            cekAktifitas();
        } else {
            System.out.println("Gagal mengisi energi karena drone masih menyala");
        }
    }

    void naik() {
        if (aktifitas.equals("mati")) {
            System.out.println("Drone masih dalam keadaan mati");
        } else {
            energi = energi - 5;
            kecepatan = kecepatan + 10;
            if (kecepatan > 10) {
                ketinggian = ketinggian + 10;
            }
            System.out.println("drone naik");
        }
        cekAktifitas();

    }

    void mundur() {
        if (aktifitas.equals("mati")) {
            System.out.println("Drone masih dalam keadaan mati");
        } else {
            energi = energi - 5;
            System.out.println("drone mundur");
        }
        cekAktifitas();
    }

    void belok() {
        if (aktifitas.equals("mati")) {
            System.out.println("Drone masih dalam keadaan mati");
        } else {
            energi = energi - 5;
            System.out.println("membelokkan drone");
        }
        cekAktifitas();
    }

    void maju() {
        if (aktifitas.equals("mati")) {
            System.out.println("Drone masih dalam keadaan mati");
        } else {
            energi = energi - 5;
            System.out.println("drone maju");
        }
        cekAktifitas();
    }

    private void cekAktifitas() {

        if (kecepatan >= 10) {
            aktifitas = "terbang";
        }

        if (kecepatan <= 9 || ketinggian <= 0) {
            aktifitas = "mendarat";
        }


        if (energi <= 0) {
            kecepatan = 0;
            ketinggian = 0;
            aktifitas = "mati";
            System.out.println("Energi drone habis, silahkan isi daya terlebih dahulu");
        }

        if (kecepatan == 0) {
            aktifitas = "mati";
        }

    }
}
